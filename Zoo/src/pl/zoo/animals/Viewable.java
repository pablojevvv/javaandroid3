package pl.zoo.animals;

public interface Viewable extends AnimalInterface {
	public int getViews();
}
