package pl.sdacademy;

/**
 * Created by pablojev on 03.07.2017.
 */
public class IronMan extends SuperHero implements Flyable, Overpowered {
    @Override
    public void superPower() {
        System.out.println("Potrafię latać!");
    }

    @Override
    public void fly() {
        System.out.println("\tIronMan super lata!");
    }

    @Override
    public void hit() {
        System.out.println("\tIronMan mocno uderza!");
    }
}
