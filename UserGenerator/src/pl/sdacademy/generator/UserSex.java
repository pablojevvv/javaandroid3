package pl.sdacademy.generator;

public enum UserSex {
	SEX_MALE("Mezczyzna", "im_m"), SEX_FEMALE("Kobieta", "im_f");
	
	private String slug;
	private String filename;
	
	private UserSex(String slug, String filename) {
		this.slug = slug;
		this.filename = filename;
	}
	
	public String getSlug() {
		return this.slug;
	}
	
	public String getFilename() {
		return this.filename;
	}
}
