package pl.sdacademy.calc;

@FunctionalInterface
public interface CalculatorInterface {
	public double operation(int a, int b);
	
	public default int getTwo() {
		return 2;
	}
}
