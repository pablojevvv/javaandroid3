package pl.zoo.people;

public interface PersonInterface {
	public String getName();
	public void setName(String name);
}
