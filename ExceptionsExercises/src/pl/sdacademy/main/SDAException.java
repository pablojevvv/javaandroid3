package pl.sdacademy.main;

public class SDAException extends Exception {
	SDAException(String message) {
		super(message);
	}
}
