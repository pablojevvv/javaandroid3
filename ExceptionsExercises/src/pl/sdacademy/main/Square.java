package pl.sdacademy.main;

public class Square {
	public static double square(int n) throws IllegalArgumentException {
		if(n < 0) {
			throw new IllegalArgumentException("Przekazano niepoprawny argument");
		}
		return Math.sqrt(n);
	}
}
