package pl.sdacademy.builder;

/**
 * Created by pablojev on 04.07.2017.
 */
public class User {
    private String firstName; // konieczne
    private String lastName; // konieczne
    private int age; // opcjonalne
    private String phone; // opcjonalne
    private String address; // opcjonalne

//    public User(String firstName, String lastName) {
//        this.firstName = firstName;
//        this.lastName = lastName;
//    }

    public User(UserBuilder ub) {
        this.firstName = ub.getFirstName();
        this.lastName = ub.getLastName();
        this.age = ub.getAge();
        this.phone = ub.getPhone();
        this.address = ub.getAddress();
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", phone='" + phone + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
