package pl.sdacademy.main;

public class InitialExercises {
	public int addElementsDividedBySeven(int[][] arr) {
		int sum = 0;
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				if (arr[i][j] % 7 == 0) {
					sum += arr[i][j];
				}
			}
		}
		return sum;
	}

	// 2. Napisz program obliczający iloczyn elementów tablicy dwuwymiarowej.
	public int multiplyElements(int[][] arr) {
		int result = 1;

		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				result *= arr[i][j];
			}
		}

		return result;
	}

	// 3. Napisz program obliczający iloczyn elementów parzystych tablicy
	// dwuwymiarowej.
	public int multiplyEvenElements(int[][] arr) {
		int result = 1;

		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				if (arr[i][j] % 2 == 0) {
					result *= arr[i][j];
				}
			}
		}
		return result;
	}

	// 4. Napisz program obliczający iloczyn elementów nieparzystych tablicy
	// dwuwymiarowej.
	public int multiplyOddElements(int[][] arr) {
		int result = 1;

		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				if (arr[i][j] % 2 == 1) {
					result *= arr[i][j];
				}
			}
		}

		return result;
	}

	// 5. Napisz program obliczający iloczyn elementów podzielnych przez 3
	// tablicy dwuwymiarowej.
	public int multiplyElementsDividedByThree(int[][] arr) {
		int result = 1;

		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				if (arr[i][j] % 3 == 0) {
					result *= arr[i][j];
				}
			}
		}

		return result;
	}

	// 6.
	public int find2DArrayMin(int[][] arr) {
		int min = arr[0][0];
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				if (arr[i][j] < min) {
					min = arr[i][j];
				}
			}
		}
		return min;
	}

	// 7. Napisz program znajdujący maksymalny element tablicy dwuwymiarowej.
	public int find2DArrayMax(int[][] arr) {
		int max = arr[0][0];

		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				if (max < arr[i][j]) {
					max = arr[i][j];
				}
			}
		}

		return max;

	}
	// 8. Napisz program sumujący elementy parzyste w każdym wierszu tablicy
	// dwuwymiarowej. Funkcja powinna zwracać tablicę jednowymiarową zawierającą
	// sumy poszczególnych wierszy w indeksach tablicy.

	public int[] sumEvenNumbersInRows(int[][] arr) {

		int[] result = new int[arr.length];

		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				if (arr[i][j] % 2 == 0) {
					result[i] += arr[i][j];
				}
			}
		}

		return result;
	}
}
