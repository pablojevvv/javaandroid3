package pl.sdacademy;

/**
 * Created by pablojev on 03.07.2017.
 */
public class AquaMan extends SuperHero implements Swimmable {
    @Override
    public void superPower() {
        System.out.println("Nigdy się nie topię!");
    }

    @Override
    public void swim() {
        System.out.println("\tPłynę!");
    }
}
