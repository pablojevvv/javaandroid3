package pl.sdacademy.animals;

public interface AnimalInterface {
	public void getVoice();
	public String getName();
}
