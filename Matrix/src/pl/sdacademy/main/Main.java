package pl.sdacademy.main;

public class Main {

	public static void main(String[] args) {
		int[][] input = { { 1, 2 }, { 3, 4 }, { 7, -9 }, { 21, 4 } };

		InitialExercises se = new InitialExercises();
		System.out.println("Suma elementow podzielnych przez 7 wynosi: " + se.addElementsDividedBySeven(input));
		
	}

}
