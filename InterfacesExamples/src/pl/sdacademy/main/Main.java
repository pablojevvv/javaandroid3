package pl.sdacademy.main;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import pl.sdacademy.animals.AnimalInterface;
import pl.sdacademy.animals.Cat;
import pl.sdacademy.animals.Dog;
import pl.sdacademy.calc.Calc;
import pl.sdacademy.colors.Colors;
import pl.sdacademy.threads.ThreadExample;

public class Main {

	public static void main(String[] args) {
		Dog d1 = new Dog("Burek");
		Cat c1 = new Cat("Maurycy");
		Cat c2 = new Cat("Klakier");
		
		List<AnimalInterface> animals = new LinkedList<>();
		animals.add(d1);
		animals.add(c1);
		animals.add(c2);
		String animal = "";
		Dog d = null;
		Cat c = null;
		for(AnimalInterface ai : animals) {
			
			if(ai instanceof Dog) {
				//d = (Dog) ai;
				//System.out.println(d.getName() + " robi ");
			} else if(ai instanceof Cat) {
				//c = (Cat) ai;
				//System.out.println(c.getName() + " robi ");
			}
			System.out.println(ai.getName() + " robi ");
			ai.getVoice();
			System.out.println("---------------------");
		}
		
		
		/*
		new Thread(new ThreadExample()).start();
		
		new Thread(new Runnable() {
			@Override
			public void run() {
				System.out.println("Inny napis!");
			}
		}).start();
		
		new Thread(() -> { 
			System.out.println("Przyklad #3.1");
			System.out.println("Przyklad #3.2"); 
		}).start();
		
		*/
		
		
		//Calc c = new Calc();
		//System.out.println("3 + 4 = " + c.add(3, 4));
		/*
		Calc c = new Calc(3, 4);
		System.out.println("3 + 4 = " + 
				c.doOperation((a, b) -> (a + b))
		);
		
		System.out.println("3 * 4 = " + c.doOperation((int a, int b) -> {
			return a * b;
		}));
		*/
		/*
		Colors c = new Colors();
		c.setColors(Arrays.asList("blue", "blue-violet", "yellow", "brown", "green", "black"));
		
		c.printFiltered((p) -> p.length() > 4);
		*/
	}

}
