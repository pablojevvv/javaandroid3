package pl.sdacademy.animals;

public class Cat implements AnimalInterface {

	private String name;
	
	public Cat(String name) {
		this.name = name;
	}
	
	@Override
	public void getVoice() {
		System.out.println("miau miau");
	}

	@Override
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	

}
