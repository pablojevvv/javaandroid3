package pl.zoo.animals;

public enum AnimalType {
	MAMMAL, REPTILE, BIRD, FISH;
}
