package pl.sdacademy.generator;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class UserGenerator {
	private final String path = "src/pl/sdacademy/resources/";
	private HashMap<String, ArrayList<String>> resources = new HashMap<>();
	private final List<String> files = Arrays.asList("im_m.txt", "im_f.txt", "miasta.txt", "nazwiska.txt", "ulice.txt");

	public UserGenerator() throws FileNotFoundException {
		this.readFile();
	}

	public User getRandomUser() {
		User u = new User();
		UserSex uSex = this.getRandomSex();
		u.setSex(uSex);
		u.setName(this.getRandomValueByKey(uSex.getFilename()));
		u.setLastName(this.getRandomValueByKey("nazwiska"));
		u.setAddress(this.getUserAddress());
		u.setBirth(this.getRandomBirthDate());
		return u;
	}
	
	private UserSex getRandomSex() {
		if(getRandomNumberFromRange(0, 1) == 0) return UserSex.SEX_FEMALE;
		else return UserSex.SEX_MALE;
	} 

	private String getRandomValueByKey(String key) {
		Random r = new Random();
		if(this.resources.containsKey(key)) {
			int myRandInt = r.nextInt(this.resources.get(key).size());
			return this.resources.get(key).get(myRandInt);
		}
		return null;
	}

	private void readFile() throws FileNotFoundException {
		for (String filename : this.files) {
			Scanner s = new Scanner(new File(this.path + filename));
			String key = filename.split("\\.")[0].toLowerCase();
			String myCurrentLine = "";
			ArrayList<String> list = new ArrayList<String>();

			if (!this.resources.containsKey(key)) {
				while (s.hasNextLine()) {
					myCurrentLine = s.nextLine();
					list.add(myCurrentLine);
				}
				this.resources.put(key, list);
			}
			s.close();
		}
	}
	
	private UserAddress getUserAddress() {
		return new UserAddress(	this.getRandomValueByKey("ulice"), 
								this.getRandomValueByKey("miasta"), 
								"" + this.getRandomNumberFromRange(1, 100), 
								"12-300");
	}
	
	private int getRandomNumberFromRange(int from, int to) {
		return new Random().nextInt(to - from + 1) + from;
	}
	
	private String getRandomBirthDate() {
        GregorianCalendar gc = new GregorianCalendar();
        
        int year = getRandomNumberFromRange(1900, 1999);
        int dayOfYear = getRandomNumberFromRange(1, gc.getActualMaximum(GregorianCalendar.DAY_OF_YEAR));
        gc.set(GregorianCalendar.YEAR, year);
        gc.set(GregorianCalendar.DAY_OF_YEAR, dayOfYear);
        
        return gc.get(GregorianCalendar.DAY_OF_MONTH) + "." + (gc.get(GregorianCalendar.MONTH) + 1) + "." + gc.get(GregorianCalendar.YEAR);
	}
}
