package pl.zoo.tickets;

import java.sql.Date;
import java.time.Instant;

public class Ticket {
	private double price = 10.0;
	private Date bought;
	private TicketType type;
	
	public Ticket(double price, Date bought, TicketType type) {
		super();
		this.price = price;
		this.bought = bought;
		this.type = type;
	}
	
	
}
