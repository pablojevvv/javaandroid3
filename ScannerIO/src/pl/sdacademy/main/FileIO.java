package pl.sdacademy.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileIO {
	private final String path = "src/pl/sdacademy/resources/";
	
	public int countLines(String filename) throws FileNotFoundException {
		Scanner sc = new Scanner(new File(this.path + filename));
		int i = 0;
		String currentLine = "";
		while(sc.hasNextLine()) {
			currentLine = sc.nextLine();
			if(!currentLine.equals(""))
				i++;
		}
		sc.close();
		return i;
	}
}
