package pl.zoo.people;

public class Staff implements PersonInterface {

	private String name;
	
	public Staff() {
		
	}
	
	public Staff(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}
}
