package pl.sdacademy.threads;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;

public class Main {

	// metoda main() tez rezerwuje watek
	public static void main(String[] args) {
		//Thread t = new Thread(new MyThreadExample(11), "Moj watek");
		// metoda start(), z klasy Thread uruchamia metode run() 
		// nadpisana z interfejsu Runnable 
		/*t.start();
		new Thread(new OtherThread(), "Inny watek").start();
		
		t.setPriority(Thread.MAX_PRIORITY);
		
		Thread t2 = new Thread(() -> System.out.println("Trzeci watek."));
		t2.start();
		*/
		//t2.setDaemon(true);
		/*
		System.out.println("KONIEC");
		
		System.out.println("Ilosc watkow: " + Thread.activeCount()); // sprawdzamy ile mamy watkow w obrebie naszej aplikacji

		BigInteger b = new BigInteger("10000000000001");
		b = b.add(BigInteger.TEN);
		System.out.println(b.toString());
		*/
		/*MathHelper mh = new MathHelper();
		long start = System.currentTimeMillis();
		System.out.println("Liczba 858,599,509 jest liczba pierwsza: " + mh.isPrimeWhile(858599509L));
		System.out.println("Wykonanie zajelo: " + (System.currentTimeMillis() - start) + "ms");
		
		
		BigInteger bi1 = BigInteger.valueOf(555);
		BigInteger bi2 = BigInteger.valueOf(555);
		
		System.out.println(bi1.compareTo(bi2)); 
		
		
		n % i
		
		bi1.mod(bi2).compareTo(BigInteger.ZERO) == 0*/
		
		// -1 liczba jest mniejsza
		// 0 liczby sa takie same
		// 1 liczba jest wieksza
		
		MathHelper mh = new MathHelper();
		BigInteger x = BigInteger.valueOf(3);
		
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				
			}
		});
		// 1) 2 + x
		/*BigInteger d1 = new BigInteger("2");
		d1 = d1.add(x);
		System.out.println("2 + x = " + d1.toString());
		
		long start = System.currentTimeMillis();
		System.out.println("Liczba 49979693 jest pierwsza: " + mh.isBigPrime(BigInteger.valueOf(49979693)));
		long stop = System.currentTimeMillis();
		System.out.println("Wywolanie zajelo " + (stop - start) + "ms");
		
		long start2 = System.currentTimeMillis();
		System.out.println("Liczba 49979693 jest pierwsza: " + mh.isPrimeWhile(49979693));
		long stop2 = System.currentTimeMillis();
		System.out.println("Wywolanie zajelo " + (stop2 - start2) + "ms");
*/
		List<String> input = Arrays.asList("49979693", "198491329", "314606891", "393342743", "553105243", "715225739", "899809343", "982451000653");
		
		//Long.valueOf("1231231");
		// Napiszmy kod, ktory dla kazdego elementu listy sprawdzi czas wywolania
		// poszczegolnych liczb wykorzystyujac typ prosty i typ BigInteger
		
		
		for(int i = 0; i < input.size(); i += 2) {
			final String in = input.get(i);
			new Thread(() -> System.out.println(in + " = " + mh.isBigPrime(new BigInteger(in)))).start();
			if(input.size() % 2 == 0 || i != input.size()) {
				final String in2 = input.get(i+1);
				new Thread(() -> System.out.println(in2 + " = " + mh.isBigPrime(new BigInteger(in2)))).start();
			}
		}
	}

}
