package pl.sdacademy.threads;

import java.math.BigInteger;

public class MathHelper {
	public boolean isPrime(long n) {
		// napiszmy metode, ktora sprawdzi czy liczba jest liczba pierwsza (dzieli sie tylko przez 1 i sama siebie)
		for(int i = 2; i < Math.sqrt(n); i++) {
			if(n % i == 0) {
				return false;
			}
		}
		return true;
	} 
	
	public boolean isPrimeWhile(long n) {
		int i = 2;
		while(i < n) {
			if(n % i == 0) {
				return false;
			}
			i++;
		}
		return true;
	} 
	
	public boolean isBigPrime(BigInteger n) {
		BigInteger i = BigInteger.valueOf(2);
		
		// i < n
		while(i.compareTo(n) == -1) {
			if(n.mod(i).compareTo(BigInteger.ZERO) == 0) {
				return false;
			}
			i = i.add(BigInteger.ONE); // i = i + 1
		}
		
		return true;
	}
}
