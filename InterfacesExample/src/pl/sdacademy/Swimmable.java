package pl.sdacademy;

/**
 * Created by pablojev on 03.07.2017.
 */
public interface Swimmable {

    public void swim();
}
