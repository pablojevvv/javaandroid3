package pl.sdacademy.colors;

import java.util.List;
import java.util.function.Predicate;

public class Colors {

	private List<String> colors;
	
	public Colors() {}

	public List<String> getColors() {
		return colors;
	}

	public void setColors(List<String> colors) {
		this.colors = colors;
	}
	
	public void printFiltered(Predicate<String> p) {
		for(String color : colors) {
			if(p.test(color)) {
				System.out.println(color);
			}
		}
	}
}
