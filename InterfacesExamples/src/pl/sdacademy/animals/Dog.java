package pl.sdacademy.animals;

public class Dog implements AnimalInterface {

	private String name;
	
	public Dog(String name) {
		this.name = name;
	}
	
	@Override
	public void getVoice() {
		System.out.println("How how");
	}

	@Override
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	

}
