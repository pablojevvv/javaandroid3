package pl.sdacademy;

import pl.sdacademy.builder.User;
import pl.sdacademy.builder.UserBuilder;
import pl.sdacademy.chaining.SimpleChain;
import pl.sdacademy.factory.Car;
import pl.sdacademy.factory.FactoryExample;
import pl.sdacademy.singleton.SimpleObject;
import pl.sdacademy.singleton.SingletonExample;

public class Main {

    public static void main(String[] args) {
        System.out.println("Wzorce projektowe");

        SingletonExample se = SingletonExample.getInstance();
        SingletonExample se2 = SingletonExample.getInstance();

        se.setName("Paweł");

        System.out.println(se2.getName());

        SimpleObject see = new SimpleObject();
        see.index = 100;
        SimpleObject see2 = new SimpleObject();

        System.out.println(see.index);

        // "Opel,Astra,1999"

        System.out.println(FactoryExample.getCar("Opel,Astra,1999"));

        Car myCar = FactoryExample.getCar("Ford,Focus,2007");

        System.out.println(myCar);

        UserBuilder ub = new UserBuilder("Franek", "Kimono");

        User u1 = ub.age(15).phone("1234567789").address("Słowackiego").build();
        User u2 = ub.age(80).address("Grunwaldzka").build();

        System.out.println(u1);
        System.out.println(u2);

        SimpleChain sc = new SimpleChain().setName("Paweł").setLastName("Testowy");

    }
}
