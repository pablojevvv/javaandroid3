package pl.sdacademy.main;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Main {
	public static void main(String[] args) throws IOException, ParseException {
		HTTPConnectionExample e = new HTTPConnectionExample();
		String url = "http://api.db-ip.com/v2/b620ee363136c97670e9054d4d2fa361c642c789/";
		List<String> addresses = Arrays.asList("164.12.33.45", "174.12.33.45", "127.0.0.127", "172.217.23.46", "192.168.12.34", "185.11.130.82", "104.192.143.3", "145.237.193.133");
		
		for(String ip : addresses) {
			String myCurrentResponse = e.sendGET(url + ip);
			JSONParser jp = new JSONParser();
			JSONObject jo = (JSONObject) jp.parse(myCurrentResponse);
			System.out.println("[ " + ip + " ] Panstwo: " + jo.get("countryName"));
		}
		
		/*
		Map<String, String> map = new HashMap<>();
		map.put("klucz", "wartosc");
		map.put("innyKlucz", "innaWartosc");
				
		for(String key : map.keySet()) {
			System.out.println("Dla klucza: " + key + " wartosc wynosi: " + map.get(key));
		}*/
		/*HTTPConnectionExample c = new HTTPConnectionExample();
		
		try {
			//System.out.println(c.sendGET("http://wp.pl"));
			System.out.println(c.sendPOST("http://palo.ferajna.org/sda/wojciu/json.php"));
		} catch (IOException e) {
			e.printStackTrace();
		}*/
	}
}
