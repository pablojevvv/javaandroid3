package pl.zoo.animals;

public class Turtle implements Viewable {

	private AnimalType type = AnimalType.REPTILE;
	private int views = 0;
	
	
	public Turtle(AnimalType type, int views) {
		super();
		this.type = type;
		this.views = views;
	}

	@Override
	public AnimalType getType() {
		return type;
	}

	@Override
	public int getViews() {
		return views;
	}

}
