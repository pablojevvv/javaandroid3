package pl.sdacademy.singleton;

/**
 * Created by pablojev on 04.07.2017.
 */
public class SingletonExample {

    private static SingletonExample _instance = null;
    private String _name;

    private SingletonExample() {
        System.out.println("[Contruct] Utworzono nową instancję.");
    }

    public static SingletonExample getInstance() {
        // leniwa inicjalizacja
        if(_instance == null) {
            _instance = new SingletonExample();
        } else {
            System.out.println("Zwracam instancję.");
        }
        return _instance;
    }

    public String getName() {
        return _name;
    }

    public void setName(String _name) {
        this._name = _name;
    }
}
