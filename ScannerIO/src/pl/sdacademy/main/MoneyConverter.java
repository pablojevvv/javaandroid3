package pl.sdacademy.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class MoneyConverter {
	
	private final String path = "src/pl/sdacademy/resources/currency.txt";
	
	private double readCourse(String currency) throws FileNotFoundException {
		if(currency.equalsIgnoreCase("PLN")) return 1;
		Scanner s = new Scanner(new File(this.path));
		String currentLine = "";
		while(s.hasNextLine()) {
			currentLine = s.nextLine(); // 1 USD		2.12345
			String[] currencyArray = currentLine.split("\t");
			String[] currencyUnit = currencyArray[0].split(" ");
			double newCourse = Double.parseDouble(currencyUnit[0]);
			if(currencyUnit[1].equalsIgnoreCase(currency)) {
				return Double.parseDouble(currencyArray[1].replace(",", ".")) / newCourse;
			}
		}
		return 0;
	}
	
	public double convert(double money, String to) throws FileNotFoundException {
		return this.convert(money, to, "PLN");
	}
	
	public double convert(double money, String to, String from) throws FileNotFoundException {
		double fromCourse = this.readCourse(from);
		double toCourse = this.readCourse(to); 
		System.out.println(from + " " + fromCourse);
		System.out.println(to + " " + toCourse);
		return (fromCourse * money) * toCourse;
	}
}
