package pl.sdacademy;

import java.util.LinkedList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Animal a = new Animal();

        a.addAnimal("Słoń");
        a.addAnimal("Hipopotam");
        a.addAnimal("kot_pies_bóbr", animName -> animName.split("_"));
        a.addAnimal("jeż|słowik", anims -> anims.split("\\|"));

        for(String anim : a.getAnimals()) {
            System.out.println(anim);
        }

        System.exit(0);

        Book b = new Book();
        b.addBook("Pan Tadeusz");
        b.addBook("Ogniem i mieczem");

        b.showBooks(null);

        // void print(String title);
        System.out.println("#1\n");
        b.showBooksTwo(new BookInterface() {
            @Override
            public void print(String title) {
                System.out.println(title);
            }
        });

        System.out.println("#2\n");
        b.showBooksTwo((String title) -> {
            System.out.println(title);
        });

        System.out.println("#3\n");
        b.showBooksTwo((title) -> {
            System.out.println(title);
        });



        System.out.println("#4\n");
        b.showBooksTwo( title -> System.out.println(title.toUpperCase()) );



        List<SuperHero> superheroes = new LinkedList<>();
        superheroes.add(new Hulk());
        superheroes.add(new Sonic());
        superheroes.add(new IronMan());
        superheroes.add(new Herkules());
        superheroes.add(new AquaMan());

        System.out.println("");

        for(SuperHero hero : superheroes) {
            if(hero instanceof SuperHero)
                hero.superPower();

            if(hero instanceof Flyable) {
                ((Flyable) hero).fly();
            }

            if(hero instanceof Overpowered) {
                ((Overpowered) hero).hit();
            }

            if(hero instanceof Swimmable) {
                ((Swimmable) hero).swim();
            }
        }
    }
}
