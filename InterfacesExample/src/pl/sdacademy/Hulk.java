package pl.sdacademy;

/**
 * Created by pablojev on 03.07.2017.
 */
public class Hulk extends SuperHero implements Overpowered {
    @Override
    public void superPower() {
        System.out.println("Jestem bardzo silny!");
    }

    @Override
    public void hit() {
        System.out.println("\tHulk jest supersiłacz!");
    }
}
