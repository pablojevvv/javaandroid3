package pl.sdacademy.factory;

/**
 * Created by pablojev on 04.07.2017.
 */
public class FactoryExample {

    public static Car getCar(String carFields) {
        String[] fields = carFields.split(",");
        Car c = new Car();
        c.setCompany(fields[0]);
        c.setModel(fields[1]);
        c.setYear(Integer.parseInt(fields[2]));
        return c;
    }

}
