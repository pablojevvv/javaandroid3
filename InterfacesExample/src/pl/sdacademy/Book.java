package pl.sdacademy;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pablojev on 03.07.2017.
 */
public class Book {
    private List<String> books = new ArrayList<>();


    public List<String> getBooks() {
        return books;
    }

    public void setBooks(List<String> books) {
        this.books = books;
    }

    public void showBooks(BookInterface bi) {
        for(String book : books) {
            System.out.println(book);
        }
    }

    public void showBooksTwo(BookInterface bi) {
        for(String book : books) {
            bi.print(book);
        }
    }


    public void addBook(String book) {
        this.books.add(book);
    }
}
