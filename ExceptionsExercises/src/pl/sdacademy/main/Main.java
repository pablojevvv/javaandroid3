package pl.sdacademy.main;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws SDAException {
		double n = 4.2;
		
		if(n % 1 == 0) {
			System.out.println("Liczba jest calkowita");
		} else {
			System.out.println("Liczba jest zmiennoprzecinkowa");
		}
		
		System.exit(0);
		
		
		// deklaracja zmiennej tablicowej do przetrzymywania danych od uzytkownika
		int[] array = new int[10];
		// deklaracja instancji Scannera
		Scanner sc = new Scanner(System.in);
		// prosimy uzytkownika o podanie 10 liczb, ktore przypisujemy pod kolejne indeksy tablicy
		for(int i = 0; i < 10; i++) {
			System.out.println("Podaj array["+i+"] = ");
			array[i] = 0;//sc.nextInt();
		}
				
		// probujemy wyswietlic wartosc znajdujaca sie pod indeksem 10 naszej tablicy
		try {
			System.out.println("array[10] = " + array[10]);
			
			// nasluchujemy wyjatek przekroczenia wymiarow tablicy
		} catch(ArrayIndexOutOfBoundsException e) {
			// jesli wyjatek wystapi, wykonaj:
			System.out.println("Brak indeksu: " + e.getMessage());
		} finally {
			// kod wykonywany zawsze - czy wyjatek wystapi czy nie
			System.out.println("Kod wykonywany niezaleznie od wystapienia wyjatku");
		}
		
//		
//		TestClass tc = new TestClass();
//		tc.testMethod();
//		
		Square s = new Square();
		try {
			System.out.println(s.square(-16));
		} catch(IllegalArgumentException e) {
			System.out.println( e.getMessage() );
		}
		
		try {
			System.out.println(Division.division(10, 0));
		} catch(IllegalArgumentException e) {
			System.out.println(e.getMessage());
		}
	}

}
