package pl.sdacademy.generator;

public class UserAddress {
	private String street;
	private String city;
	private String no;
	private String zipCode;
	
	public UserAddress() { }
	
	public UserAddress(String street, String city, String no, String zipCode) {
		this.street = street;
		this.city = city;
		this.no = no;
		this.zipCode = zipCode;
	}

	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	@Override
	public String toString() {
		return "Ulica: " + street + ", Miasto: " + city + ", Numer domu: " + no + ", Kod pocztowy: " + zipCode;
	}
	
	
	
}
