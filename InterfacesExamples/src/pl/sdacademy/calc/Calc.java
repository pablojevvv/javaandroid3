package pl.sdacademy.calc;

public class Calc {
	
	private int a;
	private int b;
	
	

	public Calc(int a, int b) {
		super();
		this.a = a;
		this.b = b;
	}



	public double doOperation(CalculatorInterface ci) {
		return ci.operation(a, b);
	}
	
	/*public int add(int a, int b) {
		return a + b;
	}
	
	public int substract(int a, int b) {
		return a - b;
	}
	
	public int multiply(int a, int b) {
		return a * b;
	}
	
	public double divide(int a, int b) {
		return (b == 0) ? 0 : a / b;
	}*/
	
}
