package pl.zoo.people;

import pl.zoo.tickets.Ticket;

public class Visitor implements PersonInterface {
	private Ticket ticket;
	private String name;
	private boolean present;

	public Visitor() {
	}

	public Visitor(String name, boolean present, Ticket ticket) {
		this.ticket = ticket;
		this.present = present;
		this.ticket = ticket;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}
}
