package pl.sdacademy.main;

public class StringUtil {
	private String param;
	
	public StringUtil(String param) {
		this.param = param;
	}
	
	public StringUtil letterSpacing() {
		String tmp = "";
		for(int i = 0; i < this.param.length(); i++) {
			tmp += this.param.charAt(i) + " ";
		}
		this.param = tmp.substring(0, tmp.length() - 1);
		return this;
	}
	
	public StringUtil reverse() {
		// 1. zadeklarujmy zmienna tymczasowa (pusta, typu String, tmp) 
		String tmp = "";
		// 2. przeiterujmy sie od dlugosci stringa this.param - 1, do 0
		for(int i = this.param.length() - 1; i >= 0; i--) {
			// a. doklejmy aktualnie odczytana wartosc do poczatku naszego ciagu znakow
			tmp += this.param.charAt(i) + "";
		}
		// 3. ustawmy wartosc this.param na wartosc tymczasowej zmiennej (tmp) 
		this.param = tmp;
		return this;
	}
	
	public StringUtil getAlphabet() {
		this.param = "";
		for(char c = 97; c < 123; c++) {
			this.param += c + "";
		}
		return this;
	}
	
	public StringUtil getFirstLetter() {
		this.param = this.param.charAt(0) + "";
		return this;
	}
	
	public StringUtil printString() {
		System.out.println("Twoj string to: " + this.param);
		return this;
	}
}
