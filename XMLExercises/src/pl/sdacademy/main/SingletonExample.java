package pl.sdacademy.main;

public class SingletonExample {

	private static SingletonExample instance;
	
	private SingletonExample() { }
	
	public static SingletonExample getInstance() {
		// lazy initialization 
		if(instance == null) {
			System.out.println("Tworze instancje!");
			instance = new SingletonExample();
		}
		System.out.println("Zwracam instancje!");
		return instance;
	}
	
	public void getText(String txt) {
		System.out.println(txt);
	}
}
