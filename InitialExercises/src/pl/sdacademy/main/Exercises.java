package pl.sdacademy.main;

import java.util.LinkedList;

public class Exercises {

	public double[] arr = { -5, 2.2, 3.011, -10, 15, -13, 123.14, -1 / 2.0 };

	public int[] zad1() {
		int[] arr = new int[6];
		for (int i = 0; i <= 5; i++) {
			arr[i] = i;
		}
		return arr;
	}

	public int[] zad2() {
		int[] arr = new int[6];
		for (int i = 0; i <= 10; i += 2) {
			// if(i % 2 == 0) {
			arr[i / 2] = i;
			// }
		}
		return arr;
	}

	public int[] zad3() {
		int[] arr = new int[11];
		for (int i = 100; i <= 130; i += 3) {
			arr[(i - 100) / 3] = i;
		}
		return arr;
	}

	public int[] zad3_2() {
		int[] arr = new int[11];
		for (int i = 100, j = 0; i <= 130; i += 3, j++) {
			arr[j] = i;
		}
		return arr;
	}

	public int[] zad4() {
		int[] arr = new int[10];
		int i = 0;
		while (i < 10) {
			arr[i] = i;
			i++;
		}
		return arr;
	}

	public int[] zad5() {
		int[] arr = new int[5];
		int i = 0;
		do {
			arr[i] = i * 2;
			i++;
		} while (i < 5);
		return arr;
	}

	public void zad8() {
		double sum = 0;
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] < 0) {
				sum = sum + arr[i]; // sum += arr[i];
			}
		}

		System.out.println("Suma liczb ujemnych z tablicy wynosi: " + sum);
	}

	public void zad9() {
		double sum = 0;
		for (int i = 0; i < this.arr.length; i++) {
			if (i % 2 != 0) {
				sum += this.arr[i];
			}
		}

		System.out.println("Suma liczb nieparzystych w tablicy wynosi: " + sum);
	}

	public void zad10() {
		double sum = 0;
		int i = 0;
		while (i < arr.length) {
			if (arr[i] > 0) {
				sum += arr[i];
			}
			i++;
		}
		System.out.println("Suma elementow dodatnich wynosi: " + sum);
	}

	public void zad11() {
		double sum = 0;
		int i = 0;
		while (i < arr.length) {
			if (arr[i] % 1 == 0) {
				sum += arr[i];
			}
			i++;
		}
		System.out.println("Suma liczb calkowitych z tablicy wynosi: " + sum);
	}

	public void printAdresses(String street, int number) {
		String cage = "A";

		// 1. przeiteruje sie przez budynki
		for (int i = 1; i <= number; i += 2) {
			// a. iteruje sie przez mieszkania
			for (int j = 1; j <= 12; j++) {
				// - sprawdzam czy numer mieszkania jest wiekszy od 6
				// | TAK | NIE
				// klatka = B klatka = A
				if (j > 6) {
					cage = "B";
				} else {
					cage = "A";
				}
				System.out.println("ul. " + street + " " + i + "/" + cage + "/" + j);
				// - drukuje aktualny adres, skladajacy sie z nazyw ulicy,
				// numeru bloku, numeru klatki i mieszkania
			}
		}
	}
	
	public void printRangeNumbers(int a, int b) { 
		// 1. Iterujemy sie od liczby a, do liczby b
		String ret = "";
		for(int i = a; i <= b; i++) {
			if(i % 2 == 0) {
				//System.out.println(i);
				ret += i + ", ";
			}
		}
		
		for(int i = b; i >= a; i--) {
			if(i % 2 != 0) {
				//System.out.println(i);
				ret += i + ", ";
			}
		}
		System.out.println(ret.substring(0, ret.length() - 2));
		//		a. sprawdzamy czy liczba jest parzysta
		//				| TAK
		//			drukujemy ja
		// 2. Iterujemy sie od liczba b, do licbzy a (zwracamy uwage na dekrementacje)
		//		a. sprawdzamy czy liczba jest nieparzysta
		//				| TAK
		//			drukujemy ja
	}
	
	public void printRangeNumbers_2(int a, int b) { 
		LinkedList<Integer> list = new LinkedList<>();
		for(int i = a; i <= b; i++) {
			if(i % 2 == 0) {
				list.add(i);
			}
		}
		
		for(int i = b; i >= a; i--) {
			if(i % 2 != 0) {
				list.add(i);
			}
		}
		//drukowanie listy
	}

	public void printArray(int[] arr) {
		for (int i = 0; i < arr.length; i++) {
			System.out.println("arr[" + i + "] = " + arr[i]);
		}
	}
}
