package pl.sdacademy.test;

import static org.junit.Assert.*;

import org.junit.Test;

import pl.sdacademy.main.SimpleCalc;

public class CalcTest {

	@Test
	public void testAddSimpleCalcMethod() {
		SimpleCalc sc = new SimpleCalc();
		int a = 3;
		int b = 4;
		int result = 7;
		//assertTrue(result == sc.add(a, b));
		assertEquals(result, sc.add(a, b));
		
		//System.setIn();
	}

}
