package pl.sdacademy.main;

public class Division {
	public static double division(double a, double b) throws IllegalArgumentException {
		if(b == 0) {
			throw new IllegalArgumentException("Przekazano niepoprawny argument");
		}
		return a / b;
	}
	
	/*public static double division(int a, int b) {
		return Division.division(a, b);
	}*/
}
