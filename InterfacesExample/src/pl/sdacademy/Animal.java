package pl.sdacademy;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by pablojev on 03.07.2017.
 */
public class Animal {

    private List<String> animals = new LinkedList<>();

    public void addAnimal(String animal) {
        this.animals.add(animal);
    }

    public void addAnimal(String animal, AnimalInterface ai) {
        String[] anims = ai.add(animal);
        for(String anim : anims) {
            this.animals.add(anim);
        }
    }

    public boolean removeAnimal(String animal) {
        if(animals.contains(animal)) {
            animals.remove(animal);
            return true;
        }
        return false;
    }

    public List<String> getAnimals() {
        return animals;
    }
}
