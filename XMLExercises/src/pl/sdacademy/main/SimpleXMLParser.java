package pl.sdacademy.main;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.activation.DataSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class SimpleXMLParser {
	
	private final String path = "src/pl/sdacademy/resources/";
	
	public void readXMLFile(String filename) throws ParserConfigurationException, SAXException, IOException {
		// tworzymy obiekt reprezentujacy nasz plik
		File f = new File(this.path + filename);
		
		// tworzymy instancje DBF, którą pobieramy za pomoca metody statycznej newInstance()
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		// tworzymy nowy DocumentBuilder z wykorzystaniem statycznej metody newDocumentBuilder()
		DocumentBuilder db = dbf.newDocumentBuilder();
		// parsujemy nasz plik XML i reprezentujemy go za pomoca Document'u
		Document doc = db.parse(f);
		
		// dokonujemy normalizacji dokumentu, sprawdzamy poprawnosc tagow 
		doc.getDocumentElement().normalize();
		
		// tworzymy liste zawierajaca elementy <staff>
		NodeList nList = doc.getElementsByTagName("staff");
		for(int i = 0; i < nList.getLength(); i++) {
			Node n = nList.item(i);
			if(n.getNodeType() == Node.ELEMENT_NODE) {
				Element elem = (Element) n;
				
				String staffName = elem.getElementsByTagName("firstname").item(0).getTextContent() + " "
						+ elem.getElementsByTagName("lastname").item(0).getTextContent();
				
				System.out.println( staffName );
				System.out.println("===========");
			}
		}
		
	}
	
	public void writeXMLFile(String filename) throws ParserConfigurationException, TransformerException {
		// tworzymy instancje 
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.newDocument();
		
		Element root = doc.createElement("root");
		
		Element child = doc.createElement("children");
		child.setAttribute("id", "15");
		child.setTextContent("wartosc");
		
		Element child2 = doc.createElement("children");
		child2.setAttribute("id", "16");
		child2.setTextContent("wartosc druga");
		
		root.appendChild(child);
		root.appendChild(child2);
		
		doc.appendChild(root);
		
		
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer t = tf.newTransformer();
		
		DOMSource source = new DOMSource(doc);
		StreamResult sr = new StreamResult(new File(this.path + filename));
		t.transform(source, sr);
	} 
	
	// zadanie 1
	public double getAvgSalary(String filename) throws ParserConfigurationException, SAXException, IOException {
		File f = new File(this.path + filename);
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(f);
		
		doc.getDocumentElement().normalize();
		double avg = 0;
		NodeList nList = doc.getElementsByTagName("staff");
		for(int i = 0; i < nList.getLength(); i++) {
			Node n = nList.item(i);
			if(n.getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) n;
				avg += Double.parseDouble(e.getElementsByTagName("salary").item(0).getTextContent());
			}
		}
		avg /= nList.getLength();
		return avg;
	}

	public void writeStudents(String filename, List<Student> s) throws ParserConfigurationException, TransformerException {
		// tworzymy instancje 
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.newDocument();
		
		Element root = doc.createElement("root");
		Element students = doc.createElement("students");
		
		for(Student myStud: s) {
			Element currentStudent = doc.createElement("student");
			Element name = doc.createElement("name");
			Element lastname = doc.createElement("lastname");
			Element year = doc.createElement("year");
			name.setTextContent(myStud.getName());
			lastname.setTextContent(myStud.getLastname());
			year.setTextContent(myStud.getYear() + "");
			currentStudent.appendChild(name);
			currentStudent.appendChild(lastname);
			currentStudent.appendChild(year);
			students.appendChild(currentStudent);
		}
		
		root.appendChild(students);
		doc.appendChild(root);
		
		
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer t = tf.newTransformer();
		
		DOMSource source = new DOMSource(doc);
		StreamResult sr = new StreamResult(new File(this.path + filename));
		t.transform(source, sr);
	} 
	
	public void printFilenames(String file, int from, int to) {
		for(int i = from; i <= to; i++) {
			String filename = file + "_" + i + ".xml";
			System.out.println(filename);
		}
	}
	
}
