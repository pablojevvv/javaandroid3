package pl.sdacademy.main;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class Main {

	public static void main(String[] args) {
		
		/*TwoDimArrays tda = new TwoDimArrays();
		int[][] arr = { { 21, 4 }, { -14, 13 } };
		System.out.println("Suma liczb podzielnych przez 7: " + tda.addElementsDividedBySeven(arr));
		
		
		System.exit(0);*/
		/* Deklaracja zmiennych calkowitych */
		int a = 5;
		Integer b = 7;
		
		
		/* 5 > 10 - false */
		if(a > 10) {
			System.out.println("Twoja liczba jest wieksza od 10");
		} else {
			System.out.println("Twoja liczba jest mniejsza lub rowna 10");
		}
		
		// if
		// if..elseif
		// if..elseif..elseif..else
		// if..else
		// if..elseif..else
		
		//  a < 9 - true
		//  b == 7 - true
		//  a & b - true
		if(a < 9 && b == 7) {
			System.out.println("Warunki zostaly spelnione!");
		} 
		
		// a == 6 - false
		// b == 8 - false
		// a | b - false
		if(a == 6 || b == 8) {
			System.out.println("[ 2 ] Drugi warunek takze jest spelniony");
		}
		
		/* jezeli mamy warunek, w ktorym modyfikujemy ta sama zmienna, to mozemy zapisac go krocej */
		if(a == 5) {
			b = 10;
		} else {
			b = 15;
		}
		
		// zapis short if
		b = (a == 5) ? 10 : 15;
		
		Integer c = 7;
		
		// taki zapis jest juz nieczytelny, wiec raczej go nie stosujemy
		b = (a == 5) ? ((b == 8) ? 20 : 25) : ((c == 7) ? 30 : 35);
		
		
		// przelaczaj zmienna 'c' i sprawdzaj jej wartosc:
		switch(c) {
			// jesli jest rowna 1, to:
			case 1:
				System.out.println("Mamy jedynke!");
				break;
			// jesli jest rowna 2, to:
			case 2:
				System.out.println("Mamy dwojke!");
				break;
			case 7:
				System.out.println("Mamy siodemke!");
				break;
			default:
				System.out.println("Nie znalazlem takiej wartosci!!");	
		}
		
		/* Petle, tablice */
		
		int[] arrayOfInts = { 1, 3, 14, 22, -17, -24 };
		
		// TABLICA ZAWSZE MA ZADEKLAROWANY ROZMIAR!
		
		System.out.println(arrayOfInts.length); // 6 - liczba elementow w tablicy
		// elementy w tablicy znajduja sie pod indeksami, indeksy zaczynają się od 0.
		
		System.out.println(arrayOfInts[0]);
		System.out.println("-----------------------------");
		
		int sum = 0; // zero nie zmienia wyniku sumy
		
		for(int i = 0; i < arrayOfInts.length; i++) {
			sum += arrayOfInts[i];
			System.out.println(arrayOfInts[i]);
		}
		
		System.out.println("Suma elementow wynosi: " + sum);
		
		// Tablice dwuwymiarowe
		
		int[][] nestedArray = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
		
		for(int i = 0; i < nestedArray.length; i++) {
			for(int j = 0; j < nestedArray[i].length; j++) {
				System.out.printf("%4s", nestedArray[i][j]);
				
				// %s - string
				// %d - int
				// %f, %lf - double, float
				// %c - char
				
				// %4s - string, ktory zawsze zajmie minimum 4 miejsca
				// %4.2f - liczba typu float, zawierajaca reprezentacje 4 miejsc lacznie i 2 po przecinku
			}
			System.out.println("");
		}
		
		// Petla while - podczas gdy
		
		int n = 0;
		
		while(n < 10) {
			System.out.println("Twoje n wynosi: " + n);
			n++;
		}
		
		// Petla do..while - rob..dopoki
		
		// petla ta wykona sie zawsze co najmniej raz
		
		int m = 5;
		
		do {
			System.out.println("Twoje m wynosi: " + m++);
		} while(m < 10);
		
		// inkrementacja
		
		int k = 5; 
		
		System.out.println(++k); // 6
		System.out.println(k); // 6
		
		// dekrementacje
		
		System.out.println(k--); // 6
		System.out.println(k); // 5
		
		/* KOLEKCJE */
		System.out.println("===============");
		System.out.println("K O L E K C J E");
		System.out.println("===============");
		
		// LISTA 
		System.out.println("");
		
		// tylko raz informujemy kompilator, o tym jakiego typu dane bedziemy przetrzymywali na naszych listach
		List<String> listOfStrings = new LinkedList<>();
		
		// lista jest struktura danych 
		// listy sa uporzadkowane
		// listy zachowuja kolejnosc danych
		// listy maja dynamiczny rozmiar - automatycznie sie rozszerzaja
		// listy nie zachowuja unikalnosci przetrzymywanych danych
		// elementy zawsze dodajemy na koncu listy (wyjatki shift, unshift)
		// kolekcje nie przyjmuja typu prostego
		
		listOfStrings.add("Pierwszy string"); // metoda 'add' dodaje do listy - jesli lista jest pusta, to umieszcza wartosc pod indeksem 0
		listOfStrings.add("Drugi string"); // indeks: 1
		listOfStrings.add("Trzeci string"); // indeks: 2
		
		for(int i = 0; i < listOfStrings.size(); i++) {
			System.out.println(listOfStrings.get(i)); // metoda 'get' pobiera wartosc z listy
		}
		
		listOfStrings = Arrays.asList("blue", "violet", "yellow", "brown", "red", "green", "yellow"); // wykorzystanie metody asList z klasy Arrays
		
		for(String color : listOfStrings) {
			System.out.println("Kolejny kolor: " + color);
		}
		
		
		CollectionsEx ce = new CollectionsEx();
		System.out.println("Suma elementow listy podzielnych przez 4 to: " + ce.sumAllElementsDividedByFour());
		
		// ZBIOR
		System.out.println("");
		
		Set<String> setOfStrings = new HashSet<>();
		setOfStrings.add("Pawel"); // 0
		setOfStrings.add("Kamil"); // 1
		setOfStrings.add("Marcin"); // 2
		setOfStrings.add("Pawel"); // 3
		// zbior przechowuje wartosci unikalne (!) - tylko i wylacznie
		// zbior nie zachowuje kolejnosci danych
		// zbior jest nieuporzadkowany
		// (!) ze zbioru nie wybieramy po indeksie, gdyz jest nieuporzadkowany
		
		for(String name : setOfStrings) {
			System.out.println("Kolejne imie: " + name);
		}
		
		// zbior to nieuporzadkowana lista, ktora zawiera wartosci unikalne
	
		Set<String> otherSet = new HashSet<>();
		otherSet.addAll(setOfStrings);
		otherSet.add("Patryk");
		
		// metoda 'contains' sprawdza czy wartosc wystepuje w zbiorze
		if(otherSet.contains("Pawel")) {
			System.out.println("Pawel jest w zbiorze!");
		}
		
		if(otherSet.contains("Piotr")) {
			System.out.println("Piotr jest w zbiorze!");
		} else {
			System.out.println("Piotra nie ma w zbiorze!");
		}
		
		for(int i = 0; i < otherSet.size(); i++) {
			// metoda 'remove' usuwa wartosc ze zbioru (zwraca true lub false w zaleznosci czy element byl i zostal usuniety czy go nie bylo)
			System.out.println("imie: " + otherSet.remove("Piotr"));
		}
		
		// MAPY
		System.out.println("");
		
		// mapy to implementacja tablic asocjacyjnych
		// tablica asocjacyjna to taka tablica, ktora przetrzymuje wartosci w parach <klucz, wartosc>
		// przy zachowaniu unikalnosci klucza
		
		// wybieramy elementy po kluczu
		// mapa jest nieuporzadkowana
		
		Map<String, String> mapOfStrings = new HashMap<>();
		// mapa przyjmuje dwa argumenty: typ klucza i typ wartosci
		mapOfStrings.put("klucz", "wartosc");
		mapOfStrings.put("k1", "v1");
		mapOfStrings.put("k2", "v2");
		
		// znam klucz, wyswietlam pojedyncza wartosc przypisana do tego klucza
		System.out.println(mapOfStrings.get("klucz"));
		
		mapOfStrings.put("klucz", "inna wartosc"); // nadpisanie wartosci dla klucza - dowodzi o unikalnosci klucza
		
		// skoro klucze z mapy sa zbiorem (Set), to oznacza to, ze mapa musi zawierac tylko i wylacznie unikalne pary kluczy
		
		// iteracja przez mape
		// 1. Znac wszystkie klucze, ktore wystepuja w mapie
		Set<String> keysFromMap = mapOfStrings.keySet(); // metoda keySet zwraca zbior kluczy
		// 2. iteracja po wszystkich kluczach
		for(String key : keysFromMap) {
			// 3. wybranie wartosci po aktualnym kluczu
			System.out.println("Dla klucza: " + key + " wartosc wynosi: " + mapOfStrings.get(key));
		}
				
		// Wczytanie danych od uzytkownika
		Scanner s = new Scanner(System.in);
		
		/* wczytanie tekstu */
		System.out.println("Podaj stringa: ");
		String myLine = s.nextLine();
		
		/* wczytanie liczby */
		System.out.println("Podaj int: ");
		int num = s.nextInt();
		
		System.out.println("Tekst: " + myLine);
		System.out.println("Liczba: " + num);
		
		
	}

}
