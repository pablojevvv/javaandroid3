package pl.zoo.init;

import java.util.LinkedList;
import java.util.List;

import pl.zoo.animals.AnimalInterface;
import pl.zoo.animals.AnimalType;
import pl.zoo.animals.Turtle;
import pl.zoo.people.Staff;
import pl.zoo.people.Visitor;
import pl.zoo.tickets.Ticket;
import pl.zoo.tickets.TicketType;

public class Zoo {
	
	private List<Staff> staff = new LinkedList<>(); 
	private List<Visitor> visitors = new LinkedList<>(); 
	private List<AnimalInterface> animals = new LinkedList<>(); 
	
	private int numOfPeople = 0;
	
	public Zoo(int numOfPeople) { }
	
	public void init() { }
	
	public void addVisitors() { }
	
	public void addAnimals() { }
	
	public void addStaff() { }
	
	public void visit(Visitor visitor, AnimalInterface animal) { }
	
}
