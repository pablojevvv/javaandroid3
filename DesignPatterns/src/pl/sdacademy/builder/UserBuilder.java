package pl.sdacademy.builder;

/**
 * Created by pablojev on 04.07.2017.
 */
public class UserBuilder {
    private String firstName; // konieczne
    private String lastName; // konieczne
    private int age; // opcjonalne
    private String phone; // opcjonalne
    private String address; // opcjonalne

    public UserBuilder(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public UserBuilder age(int age) {
        this.age = age;
        return this;
    }

    public UserBuilder phone(String phone) {
        this.phone = phone;
        return this;
    }

    public UserBuilder address(String address) {
        this.address = address;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public User build() {
        User u = new User(this);
        age = 0;
        address = null;
        phone = null;
        return u;
    }
}
