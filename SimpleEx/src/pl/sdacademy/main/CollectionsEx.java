package pl.sdacademy.main;

import java.util.Arrays;
import java.util.List;

public class CollectionsEx {
	
	private List<Integer> myArgs = Arrays.asList(12, 16, 64, 36, 4, 32, 7, 3, 2, 9, 5);
	
	public Integer sumAllElementsDividedByFour() {
		// Napisz metode, sumujaca i zwracajaca sume wszystkich liczb z listy myArgs, ktore sa podzielne przez 4
		int sum = 0; // 0 nie wplywa na wynik dodawania / odejmowania
		for(int i = 0; i < myArgs.size(); i++) {
			if(myArgs.get(i) % 4 == 0) {
				sum += myArgs.get(i);
			}
		}
		return sum;
	}
}
