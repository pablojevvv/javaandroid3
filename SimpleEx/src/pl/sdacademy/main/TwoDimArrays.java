package pl.sdacademy.main;

public class TwoDimArrays {
	public int addElementsDividedBySeven(int[][] arr) {
		// deklarujemy zmienna przetrzymujaca sume liczb podzielnych przez 7
		int sum = 0;
		// iterujemy sie przez pierwsza tablice 'arr' (kazdy 'wybrany' element bedzie tablica)
		for(int i = 0; i < arr.length; i++) {
			// iterujemy sie przez arr[i] (kolejna tablice)
			for(int j = 0; j < arr[i].length; j++) {
				// sprawdzamy, czy aktualnie wybrana liczba jest podzielna przez 7
				if(arr[i][j] % 7 == 0) {
					// jesli tak, dodajemy ja do sumy
					sum += arr[i][j];
				}
			}
		}
		// zwracamy sume
		return sum;
	}
	
	public int multiplyElements(int[][] arr) {
		int multi = 1;
		for(int i = 0; i < arr.length; i++) {
			for(int j = 0; j < arr[i].length; j++) {
				multi *= arr[i][j]; // 'multi *= liczba' jest tym samym co 'multi = multi * liczba;'
			}
		}
		return multi;
	}
	
	public int multiplyEvenElements(int[][] arr) {
		int multi = 1;
		for(int i = 0; i < arr.length; i++) {
			for(int j = 0; j < arr[i].length; j++) {
				if(arr[i][j] % 2 == 0) multi *= arr[i][j]; 
			}
		}
		return multi;
	}

	public int multiplyOddElements(int[][] arr) {
		int multi = 1;
		for(int i = 0; i < arr.length; i++) {
			for(int j = 0; j < arr[i].length; j++) {
				if(arr[i][j] % 2 != 0) multi *= arr[i][j]; 
			}
		}
		return multi;
	}
	
	public int multiplyElementsDividedByThree(int[][] arr) {
		int multi = 1;
		for(int i = 0; i < arr.length; i++) {
			for(int j = 0; j < arr[i].length; j++) {
				if(arr[i][j] % 3 == 0) multi *= arr[i][j];
			}
		}
		return multi;
	}
	
	public int find2DArrayMin(int[][] arr) {
		int min = arr[0][0];
		for(int i = 0; i < arr.length; i++) {
			for(int j = 0; j < arr[i].length; j++) {
				if(arr[i][j] < min) min = arr[i][j];
			}
		}
		return min;
	}
	
	public int find2DArrayMax(int[][] arr) {
		int max = arr[0][0];
		for(int i = 0; i < arr.length; i++) {
			for(int j = 0; j < arr[i].length; j++) {
				if(arr[i][j] > max) max = arr[i][j];
			}
		}
		return max;
	}
	
	public int[] sumEvenNumbersInRows(int[][] arr) {
		int[] retArray = new int[arr.length];
		for(int i = 0; i < arr.length; i++) {
			int sum = 0;
			for(int j = 0; j < arr[i].length; j++) {
				if(arr[i][j] % 2 == 0) sum += arr[i][j];
			}
			retArray[i] = sum;
		}
		return retArray;
	}

}
