package pl.sdacademy.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class ScannerIO {
	
	// pole przetrzymujace sciezke do katalogu resources, w ktorym przetrzymujemy pliki do odczytu / zapisu
	public static final String path = "src/pl/sdacademy/resources/";

	public static void main(String[] args) throws FileNotFoundException {
		
		
		//MoneyConverter mc = new MoneyConverter();
		//System.out.println(mc.convert(1000, "JPY", "PLN"));
		FileIO f = new FileIO();
		System.out.println(f.countLines("tempC.txt"));
		
		/* WCZYTANIE LICZBY Z KLAWIATURY */
		
		// deklarujemy zmienna sc typu Scanner, ktorej jako argument konstruktora przekazujemy 
		// Strumien danych - w naszym przypadku bedzie to 'standardowe wejscie'
		// w javie standardowo jest reprezentowane poprzez System.in
		Scanner sc = new Scanner(System.in);
		// prosimy uzytkownika o podanie liczby
		System.out.println("Podaj liczbe calkowita: ");
		// przypisujemy do zmiennej n wartosc podana przez uzytkownika i sparsowana na int
		int n = sc.nextInt();
		// drukujemy informacje ile wynosi podowojona wartosc liczby podanej przez uzytkownika
		System.out.println("Twoja liczba * 2 = " + (n * 2));
		
		/* WCZYTANIE LICZB Z PLIKU */
		// deklarujemy zmienna przetrzymujaca sume liczb
		int sum = 0;
		// deklarujemy Scanner w naglowku try - pozwoli to nam na automatyczne zamkniecie Scannera
		// try with resources (interfejs closeable)
		try(Scanner scc = new Scanner(new File(ScannerIO.path + "data.txt"))) {
			// wykonujemy dzialania, dopoki w pliku sa kolejne linie
			while(scc.hasNextLine()) {
				// do zmiennej sum dodajemy przeparsowana zawartosc kazdej kolejnej linii na liczbe calkowita
				sum += scc.nextInt();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		// drukujemy na ekran sume wartosci z pliku
		System.out.println("Suma wartosci we wierszach z pliku to: " + sum);
		
		/* ZAPIS DANYCH DO PLIKU */
		try {
			// deklarujemy obiekt PrintWriter, ktory posluzy nam do zapisu danych do pliku
			// jako argument konstruktora przekazujemy nowa instancje FileWritera, ktora przyjmuje
			// obiekt typu File (mogacy wyrzucic IOException)
			
			// jezeli dla instancji FileWriter'a dodamy drugi parametr ustawiony jako 'true'
			// to bedziemy dodawali do pliku, a nie nadpisywali go od poczatku
			// czyli w skrocie dokonamy appendu do pliku
			PrintWriter pw = new PrintWriter(new FileWriter(new File(ScannerIO.path + "plik.txt"), true));
			// zapisujemy nowa linie do pliku "przykladowy plik"
			pw.println("przykladowy plik");
			// zapisujemy kolejna linie
			pw.println("inny string");
			// zamykamy plik
			pw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
