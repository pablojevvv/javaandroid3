package pl.sdacademy;

/**
 * Created by pablojev on 03.07.2017.
 */
public interface Flyable {
    public void fly();
}
