package pl.sdacademy.generator;

public class User {
	
	private String name;
	private String lastName;
	private UserSex sex;
	private UserAddress address;
	private String ccn;
	private String pesel;
	private String birth; // day.month.Year
	
	public User() {
		
	}

	public User(String name, String lastName, UserSex sex, UserAddress address, String ccn, String pesel, String birth) {
		this.name = name;
		this.lastName = lastName;
		this.sex = sex;
		this.address = address;
		this.ccn = ccn;
		this.pesel = pesel;
		this.birth = birth;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public UserSex getSex() {
		return sex;
	}

	public void setSex(UserSex sex) {
		this.sex = sex;
	}

	public UserAddress getAddress() {
		return address;
	}

	public void setAddress(UserAddress address) {
		this.address = address;
	}

	public String getCcn() {
		return ccn;
	}

	public void setCcn(String ccn) {
		this.ccn = ccn;
	}

	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}

	public String getBirth() {
		return birth;
	}

	public void setBirth(String birth) {
		this.birth = birth;
	}

	@Override
	public String toString() {
		return "User [name=" + name + ", lastName=" + lastName + ", sex=" + sex + ", address=" + address + ", ccn="
				+ ccn + ", pesel=" + pesel + ", birth=" + birth + "]";
	}
	
	

}
