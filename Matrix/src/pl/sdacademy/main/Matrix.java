package pl.sdacademy.main;

public class Matrix {

	// 1. Napisz funkcję drukującą tablicę dwuwymiarową, tak aby została
	// wydrukowana jako macierz.
	// Wejście:
	// int[][] m = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
	// Wyjście:
	// 1 2 3
	// 4 5 6
	// 7 8 9

	public void print2DArray(int[][] m) {

		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m[i].length; j++) {
				System.out.printf("%3d ", m[i][j]);
			}
			System.out.print("\n");
		}
	}

	// 2. Napisz funkcję, która przyjmie jako parametr tablicę dwuwymiarową i
	// przypisze 1 jako jej wartości
	// w każdym z pól - taką konstrukcję nazywamy macierzą jednostkową. Funkcja
	// powinna zwracać
	// uzupełnioną tablicę dwuwymiarową.

	public int[][] identityMatrix(int[][] m) {
		if (m.length != m[0].length) {
			for (int i = 0; i < m.length; i++) {
				for (int j = 0; j < m[i].length; j++) {
					if (i == j) {
						m[i][j] = 1;
					}
				}
			}
		}

		return m;
	}

	// 3. Napisz funkcję, która przyjmie jako parametr tablicę dwuwymiarową i
	// przypisze kolejne liczby od 1
	// jako jej wartości. Funkcja powinna zwracać uzupełnioną tablicę
	// dwuwymiarową.

	public int[][] indexedMatrix(int[][] m) {
		int counter = 1;

		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m[i].length; j++) {
				m[i][j] = counter++;
			}
		}

		return m;
	}

	// 4. Napisz funkcję sprawdzającą czy dwie przekazane jako argumenty tablice
	// dwuwymiarowe mają takie wymiary (tj. zarówno w jednej jak i w drugiej
	// tablicy ilość wierszy i kolumn jest taka sama).

	public boolean isEqualDimension(int[][] a, int[][] b) {

		if (a.length != b.length) {
			return false;
		}

		for (int i = 0; i < a.length; i++) {
			if (a[i].length != b[i].length) {
				return false;
			}
		}

		return true;
	}

	// 5. Napisz funkcję, która doda do siebie dwie macierze. Funkcja powinna
	// zwracać sumę macierzy (jako macierz). Zwróć uwagę na to, że obie macierze
	// muszą mieć takie same wymiary.

	public int[][] addMatrix(int[][] a, int[][] b) throws IllegalArgumentException {
		if (!this.isEqualDimension(a, b)) {
			throw new IllegalArgumentException("Tablice nie są jednakowej długości!");
		}

		int[][] c = new int[a.length][a[0].length];

		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[0].length; j++) {
				c[i][j] = a[i][j] + b[i][j];
			}
		}

		return c;
	}

	// 6. Napisz funkcję, która odejmie od siebie dwie macierze. Funkcja powinna
	// zwracać różnicę macierzy
	// (jako macierz). Zwróć uwagę na to, że obie macierze muszą mieć takie same
	// wymiary.

	public int[][] substractMatrix(int[][] a, int[][] b) throws IllegalArgumentException {
		if (!this.isEqualDimension(a, b)) {
			throw new IllegalArgumentException("Tablice nie są jednakowej długości!");
		}

		int[][] c = new int[a.length][a[0].length];
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[0].length; j++) {
				c[i][j] = a[i][j] - b[i][j];
			}
		}

		return c;
	}

	// 7. Napisz funkcję, która przemnoży macierz przez liczbę (każda z komórek
	// musi zostać pomnożona przez tę liczbę).
	public int[][] multiplicateMatrix(int[][] m, int n) {

		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m[i].length; j++) {
				m[i][j] *= n;
			}
		}
		return m;
	}

	// 8. Napisz funkcję, która pozowli na realizację tranpozycji macierzy
	// (tranpozycja, w skrócie zamiana wierszy z kolumnami /także jeśli chodzi o
	// wymiary/).

	public int[][] transpose(int[][] m) {
		int[][] result = new int[m[0].length][m.length];
		for (int i = 0; i < result.length; i++) {
			for (int j = 0; j < result[i].length; j++) {
				result[i][j] = m[j][i];
			}
		}
		return result;
	}

	// 9. Napisz funkcję sprawdzającą, czy macierz jest symetryczna ( tj. A(T) =
	// A ).

	public boolean isSymetric(int[][] a) {
		if (a.length != a[0].length) {
			return false;
		}

		int[][] b = this.transpose(a);

		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[0].length; j++) {
				if ((i != j) && (a[i][j] != b[j][i])) {
					return false;
				}
			}
		}

		return true;
	}

	// *10. Napisz funkcję realizującą mnożenie macierzy przez macierz.

	public int[][] multiplicateTwoMatrix(int[][] a, int[][] b) throws IllegalArgumentException {
		if (a.length != b[0].length) {
			throw new IllegalArgumentException("Tabele mają nieodpowiednie rozmiary!");
		}

		int[][] c = new int[a.length][b[0].length];

		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < c[0].length; j++) {
				for (int k = 0; k < a[0].length; k++) {
					c[i][j] += a[i][k] * b[k][j];
				}
			}
		}
		return c;

	}

	// *11. Napisz funkcję obliczającą wyznacznik macierzy 3x3 (ze sprawdzeniem
	// wymiarów przekazanej macierzy).
	
	public int indicator(int[][] m) throws IllegalArgumentException {

		if (m.length != m[0].length) {
			throw new IllegalArgumentException("Nie można wyznaczyć wyznacznika dla macierzy niekwadratowej!");
		}

		int result = 0;

		if (m.length == 1) {
			result = m[0][0];
		} else {

			for (int i = 0; i < m[0].length; i++) {
				int[][] temp = new int[m.length - 1][m[0].length - 1];
				
				for (int j = 1, jm = 0; j < m[0].length; j++) {
				
					for (int k = 0, km = 0; k < m[0].length; k++) {
						if (j != 0 && k != i) {

							temp[jm][km] = m[j][k];
							km++;
						}
					}
					
					jm++;
				
				}
	
				result += m[0][i] * Math.pow(-1, (1 + (i + 1))) * this.indicator(temp);
	
			}
		}
		return result;
	}

}