package pl.sdacademy.main;

public class Main {

	public static void main(String[] args) {
		String str1 = "a";
		String str2 = "A";
		
		System.out.println(str1 == str2);
		
		// equals
		System.out.println( str1.equals(str2) ); 
		
		// equalsIgnoreCase
		System.out.println(str1.equalsIgnoreCase("A")); // true
		
		// length
		System.out.println( str1.length() );
		
		// substring
		str1 = "oksymoron";
		System.out.println( str1.substring(0, 3) );
		//	O  K  S  Y  M  O  R  O  N
		// 0	 1  2  3  4  5  6  7  8  9
		System.out.println( str1.substring(3) );
		
		// trim 
		str1 = "   pawel   ";
		System.out.println(str1);
		System.out.println( str1.trim() );
		
		// charAt
		str1 = "test";
		System.out.println(str1.charAt(1)); // e
		
		// replace
		System.out.println( str1.replace("t", "_") );
		
		// concat
		System.out.println(str1.concat(str2)); // do str1 doklejamy str2
		
		// contains, startsWith, endsWith
		System.out.println( str1.startsWith("te") ); // rozpoczyna sie od
		System.out.println( str1.endsWith("t") ); // konczy sie na
		System.out.println( str1.contains("es") ); // zawiera
		
		// indexOf, lastIndexOf
		str1 = "Na polanie są Polanie."; // 'ni'
		System.out.println( str1.lastIndexOf("ni") );
		System.out.println( str1.indexOf("ni") );
		
		// toUpperCase, toLowerCase
		System.out.println( str1.toUpperCase() );
		System.out.println( str1.toLowerCase() );
		
		// split
		System.out.println("");
		str1 = "oksymoron";
		String[] arrOfStrings = str1.split("o");
		// oksymoron
		// "" o "ksym" o "r" o "n"
		// { "", "ksym", "r", "n" }
		
		for(String s: arrOfStrings) {
			System.out.print(s);
		}
		
		RepoName rn = new RepoName("Paneł", "Walnicki");
		System.out.println( rn.getRepoName() ); // PaweWalnJA2
		
		/*StringUtil su = new StringUtil("Test");
		su.letterSpacing().printString();*/
		new StringUtil("").getAlphabet().letterSpacing().reverse().printString();
		
		System.out.println("Czesc SDA!");
	}

}
